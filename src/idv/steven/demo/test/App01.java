package idv.steven.demo.test;

import java.util.List;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import idv.steven.demo.dao.EmployeeDao;
import idv.steven.demo.entity.Employee;

public class App01 {
	static ApplicationContext context = null;
	
	@Autowired
	private EmployeeDao empDao;

	public void run() {
		/*
		Employee emp = empDao.find("007528");
		System.out.println(emp.toString());
		*/
		
		/*
		List<Employee> all = empDao.findAll();
		for(Employee emp:all) {
			System.out.println(emp.toString());
		}
		*/

		EntityManagerFactory factory = Persistence.createEntityManagerFactory("persistenceUnit");
		Employee emp = new Employee();
		emp.setEmno("008055");
		emp.setName("Kevin");
		emp.setDepNo("015");
		emp.setArriveDate("20130206");
		emp.setInJob("Y");
		emp.setLeaveDate(" ");
		emp.setEmail("kevin@gmail.com");
		
		empDao.createEmployee(emp);
		
		factory.close();
	}

	public static void main(String[] args) {
		context = new ClassPathXmlApplicationContext("classpath:beans-config.xml");
	    App01 app01 = (App01) context.getBean("app01");
	    app01.run();
	}
}
