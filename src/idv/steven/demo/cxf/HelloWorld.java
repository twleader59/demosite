package idv.steven.demo.cxf;

import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface HelloWorld {
	String sayHello(@WebParam(name="name") String name);
}
