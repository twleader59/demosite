package idv.steven.demo.cxf;

public class HelloWorldImpl implements HelloWorld {

	@Override
	public String sayHello(String name) {
		
		return "Hello " + name;
	}

}
