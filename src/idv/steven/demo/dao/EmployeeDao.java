package idv.steven.demo.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import idv.steven.demo.entity.Employee;

@Repository
public class EmployeeDao {
	@PersistenceContext
	private EntityManager entityManager;
//
//	public List<Employee> findAll() {
//		TypedQuery<Employee> query = entityManager.createQuery("from Employee emp order by emp.emno", Employee.class);
//		
//		return query.getResultList();
//	}
//
//	public Employee find(String emno) {
//		return entityManager.find(Employee.class, emno);
//	}
//
	public void createEmployee(Employee emp) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("persistenceUnit");
        EntityManager entityManager = factory.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        
		entityManager.persist(emp);
		
		transaction.commit();
		entityManager.close();
		factory.close();
	}
}
