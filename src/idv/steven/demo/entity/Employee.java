package idv.steven.demo.entity;

import java.io.Serializable;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "steven.EMP")
public class Employee implements Serializable {
	private static final long serialVersionUID = -4849264405647999588L;
	
	@Id
	private String emno;
	private String name;
	@Column(name = "DEP_NO")
	private String depNo;
	@Column(name = "ARRIVE_DATE")
	private String arriveDate;
	private String inJob;
	@Column(name = "LEAVE_DATE")
	private String leaveDate;
	private String email;

	public String getEmno() {
		return emno;
	}
	public void setEmno(String emno) {
		this.emno = emno;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDepNo() {
		return depNo;
	}
	public void setDepNo(String depNo) {
		this.depNo = depNo;
	}
	public String getArriveDate() {
		return arriveDate;
	}
	public void setArriveDate(String arriveDate) {
		this.arriveDate = arriveDate;
	}
	public String getInJob() {
		return inJob;
	}
	public void setInJob(String inJob) {
		this.inJob = inJob;
	}
	public String getLeaveDate() {
		return leaveDate;
	}
	public void setLeaveDate(String leaveDate) {
		this.leaveDate = leaveDate;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	@Override
	public String toString() {
		return "Employee [emno=" + emno + ", name=" + name + ", depNo=" + depNo + ", arriveDate=" + arriveDate
				+ ", inJob=" + inJob + ", leaveDate=" + leaveDate + ", email=" + email + "]";
	}
}
